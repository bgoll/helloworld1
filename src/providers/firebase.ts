import { Injectable } from '@angular/core';

export interface User {
  name: string;
  role: number;
}

@Injectable()
export class fireb {
  currentUser: User;

  constructor() { }

  login(name: string, pw: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (name === 'superadmin' && pw === 'superadmin') {
        this.currentUser = {
          name: name,
          role: 0
        };
        resolve(true);
      }  else {
        resolve(false);
      }
    });
  }

  isLoggedIn() {
    return this.currentUser != null;
  }

  isAdmin() {
    return this.currentUser.role === 0;
  }

  logout() {
    this.currentUser = null;
  }
}