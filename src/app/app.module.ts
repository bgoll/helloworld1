import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AuthProvider } from '../providers/auth/auth';

import { GoogleAnalytics } from '@ionic-native/google-analytics';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { ContactPage } from '../pages/contact/contact';


import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { AddemployeePage } from '../pages/addemployee/addemployee';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule,AngularFireDatabase  } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { debutPage } from '../pages/debut/debut';
import { IonicStorageModule } from '../../node_modules/@ionic/storage';
import { environment } from './environments/environment';
import { UserSecondPage } from '../pages/user-second/user-second';
import {  contactadminPage } from '../pages/contactadmin/contactadmin';
import { ContactusPage } from '../pages/contactus/contactus';
import { AdddirecteurPage } from '../pages/adddirecteur/adddirecteur';
import { HometowPage } from '../pages/hometow/hometow';
import { AddcomercialPageModule } from '../pages/addcomercial/addcomercial.module';
import { AddcomercialPage } from '../pages/addcomercial/addcomercial';
import { HomathreePage } from '../pages/homathree/homathree';
import { HomeuserPage } from '../pages/homeuser/homeuser';
import { AddclientcommPage } from '../pages/addclientcomm/addclientcomm';
import { AddclientcommercialPage } from '../pages/addclientcommercial/addclientcommercial';
import { HomeclientcommercialPage } from '../pages/homeclientcommercial/homeclientcommercial';
import { DebutloginPage } from '../pages/debutlogin/debutlogin';
import { PermissionPage } from '../pages/permission/permission';
import { AddnotehtmlPage } from '../pages/addnotehtml/addnotehtml';
import { AddNotePage } from '../pages/add-note/add-note';
import { fireb } from '../providers/firebase';
import { SharenetworkPage } from '../pages/sharenetwork/sharenetwork';
import { HttpModule, Http } from '../../node_modules/@angular/http';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';
import { NativeStorage } from '../../node_modules/@ionic-native/native-storage';
import { Facebook } from '../../node_modules/@ionic-native/facebook';
import { LanguageService } from '../providers/auth/language.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http,'../assets/i18n/', '.json');
}

// If I use this I get error !! Argument of type 'Http' is not assignable to parameter of type 'HttpClient'. Property 'handler' is missing in type 'Http'.
// import { HttpModule, Http } from '@angular/http';






const   config = {
  apiKey: "AIzaSyBR27gNSivRImMywKDJgsZFDfCpRX-_n0E",
  authDomain: "ionicdb-a4a55.firebaseapp.com",
  databaseURL: "https://ionicdb-a4a55.firebaseio.com",
  projectId: "ionicdb-a4a55",
  storageBucket: "ionicdb-a4a55.appspot.com",
  messagingSenderId: "517463446922"
};



@NgModule({
  declarations: [
    MyApp,
    debutPage,
    HomePage,
    AddemployeePage,
    RegisterPage,
    AdddirecteurPage,
    HometowPage,
    AddcomercialPage,
    HomathreePage,
    HomeuserPage,
    AddclientcommPage,
    AddclientcommercialPage,
    HomeclientcommercialPage,
    AddnotehtmlPage

  ],
  imports: [
    BrowserModule,
    Ionic2RatingModule ,
        IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
     
      HttpModule ,
    IonicStorageModule.forRoot(),
    HttpClientModule,
 
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
],
  

  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    debutPage,
    HomePage,
   
    AddemployeePage,
    RegisterPage,
    AdddirecteurPage,
   HometowPage,
   AddcomercialPage ,
   HomathreePage,
   HomeuserPage,
   AddclientcommPage,
   AddclientcommercialPage,
   HomeclientcommercialPage,
   AddnotehtmlPage

  ],
  providers: [
    StatusBar,
    SocialSharing, 
    NativeStorage,
    Facebook,
    File,
    FileOpener,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    GoogleAnalytics,
    LanguageService
  
  ]
})
export class AppModule {}
