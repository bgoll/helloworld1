import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { debutPage } from '../pages/debut/debut';
import { PermissionPage } from '../pages/permission/permission';

import { TranslateService, LangChangeEvent } from '@ngx-translate/core';


@Component({
  templateUrl: 'app.html',
  providers: [PermissionPage]
})
export class MyApp {

rootPage:any = debutPage;

 
 constructor(platform: Platform,
public translate: TranslateService,
public statusBar: StatusBar,
public splashScreen: SplashScreen) {



platform.ready().then(() => {
  // Okay, so the platform is ready and our plugins are available.
  // Here you can do any higher level native things you might need.
  this.statusBar.styleDefault();
  this.splashScreen.hide();
  translate.setDefaultLang('en');
translate.use('en');

  //this is to determine the text direction depending on the selected language
 
});
}
}