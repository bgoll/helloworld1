import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddnotehtmlPage } from './addnotehtml';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from '../../../node_modules/angularfire2/database';
import { AngularFireAuthModule } from '../../../node_modules/angularfire2/auth';
import { HttpClientModule } from '../../../node_modules/@angular/common/http';
const config = {
  apiKey: "AIzaSyBCglLLgUuCU3BbMY2Ipo04GV2nAGeXPGI",
  authDomain: "mydata-68a83.firebaseapp.com",
  databaseURL: "https://mydata-68a83.firebaseio.com",
  projectId: "mydata-68a83",
  storageBucket: "mydata-68a83.appspot.com",
  messagingSenderId: "990020390983"
};
@NgModule({
  declarations: [
    AddnotehtmlPage,
  ],
  imports: [
    AngularFireModule.initializeApp(config),
    IonicPageModule.forChild(AddnotehtmlPage),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
      HttpClientModule,
  ],
 
})
export class AddnotehtmlPageModule {}
