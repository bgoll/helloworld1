import { Component } from '@angular/core';
import { NavController, ActionSheetController } from 'ionic-angular';
import { AngularFireDatabase ,AngularFireList  } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import firebase from 'firebase' ;
import { AddemployeePage } from '../addemployee/addemployee';
import { PermissionPage } from '../permission/permission';
import { AddNotePage } from '../add-note/add-note';


@Component({
  selector: 'page-addnotehtml',
  templateUrl: 'addnotehtml.html',
})
export class AddnotehtmlPage {

  item4: AngularFireList<any>;
 admin: Observable<any[]>;
 public id:string ;

 imageSource  ;
  employeePhoto;



  constructor(public navCtrl: NavController, af: AngularFireDatabase) {
   this.item4 =  af.list('/admin')

    
    this.admin = this.item4.snapshotChanges();

    console.log(this.admin);

  }


getPhotoURL(image){
   firebase.storage().ref().child('images/'+ image+'.jpg').getDownloadURL().then((url)=>{
   this.employeePhoto = url;

  })
}




deleteEmployee(key: string)
  {
    this.item4.remove(key);
  }

deleteItem(key: string): void {
  
 


  this.item4.remove(key) ;
  
}

editPerson(key, name, lname, age,dept) {
  console.log( key, name,  lname, dept);
   this.navCtrl.push(AddNotePage,{
   key : key,
   name : name,
   lname : lname,
  age : age,
   dept : dept
       });
   
}
  
}

