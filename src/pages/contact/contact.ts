import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddemployeePage } from '../addemployee/addemployee';

import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  email:string = '';
  password:string = '';
  
  constructor(public navCtrl: NavController, public navParams: NavParams ,public fire:AngularFireAuth) {
    }
  
  myLogIn(){
      
      this.fire.auth.signInWithEmailAndPassword(this.email, this.password).then(user=>{
              console.log(this.email +"  "+this.password)
      this.navCtrl.push( AddemployeePage)
          }).catch(function(error) {
    // Handle Errors here.
     console.log(error);
    // ...
  });  
      
      
      console.log("email:"+this.email+"password:"+ this.password)
  }
  
  }
  