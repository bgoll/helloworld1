import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeclientcommercialPage } from './homeclientcommercial';

@NgModule({
  declarations: [
    HomeclientcommercialPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeclientcommercialPage),
  ],
})
export class HomeclientcommercialPageModule {}
