import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase ,AngularFireList  } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import firebase from 'firebase' ;
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';
import { AlertController } from 'ionic-angular';
import { AddclientcommercialPage } from '../addclientcommercial/addclientcommercial';
import { PermissionPage } from '../permission/permission';

@Component({
  selector: 'page-homeclientcommercial',
  templateUrl: 'homeclientcommercial.html',
})
export class HomeclientcommercialPage {
  item4: AngularFireList<any>;
  item5: AngularFireList<any>;
  iddelete:  Observable<any[]>;
  public nm:number
  
  public totale:number

  addcommercialsclient: Observable<any[]>;
  public countryList:Array<any>;
public loadedCountryList:Array<any>;
public countryRef:firebase.database.Reference;
  private afAuth: AngularFireAuth
 imageSource  ;
  employeePhoto;
  shoppingItems: AngularFireList<any>;
 evaluation :AngularFireList<any>;
  constructor( private alertCtrl: AlertController,public navParams: NavParams,public navCtrl: NavController,
    public af: AngularFireDatabase) {
    this.item4 =  af.list('/addcommercialsclient')
      this.evaluation = af.list('/evaluation');
  
    this.nm=1 ;
      this.addcommercialsclient= this.item4.snapshotChanges();
 
    
    console.log(this.addcommercialsclient);

    var reportRef = firebase.database().ref('/iddelete/').orderByKey();
reportRef.on('child_added', function(data) {
 console.log(data.val().id); 



});

    }
 

getPhotoURL(image){
   firebase.storage().ref().child('images/'+ image+'.jpg').getDownloadURL().then((url)=>{
   this.employeePhoto = url;

  })
}
question:number=0;
getUsername()
{
 
  var reportRef = firebase.database().ref('/iddelete/'+1)

    return reportRef
 
}
public idget
public updateget
deleteEmployee(key: string)
{
  

 firebase.database().ref('/iddelete/').on('child_added', (data) =>{this.idget=data.val().id
 
  console.log(this.idget)

  });
 if(this.idget==1)
 {
     let alert = this.alertCtrl.create({
    title: 'blocked from admin',
    subTitle: 'sorry you cant delete',
    buttons: ['Dismiss']
     });
  alert.present();   
    }
    else
  



   this.item4.remove(key) ;
  
  }
editPerson(key,login,password) {

  firebase.database().ref('/idupdate/').on('child_added', (data) =>{this.updateget=data.val().id
 
    console.log(this.updateget)
  
    });
    if (this.updateget==1)
    {
      let alert = this.alertCtrl.create({
     title: 'blocked from admin',
     subTitle: 'sorry you cant update',
     buttons: ['Dismiss']
      });
   alert.present();   
     }
    else
    {
  console.log( key, login,  password);
   this.navCtrl.push(AddclientcommercialPage,{
   key : key, 
  login : login,
   password : password,
       });
      
      }

}  


initializeItems(): void {
  this.countryList = this.loadedCountryList;
}
getItems(searchbar) {
  // Reset items back to all of the items
  this.initializeItems();

  // set q to the value of the searchbar
  var q = searchbar.srcElement.value;


  // if the value is an empty string don't filter the items
  if (!q) {
    return;
  }

  this.countryList = this.countryList.filter((v) => {
    if(v.name && q) {
      if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
        return true;
      }
      return false;
    }
  });
}


addThemFunction (totale) {
     this.addcommercialsclient.subscribe(result => {
      console.log(result.length)   

  this.totale = this.nm+131+result.length  ;
  
});
   
   }
  
}
