import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AddemployeePage } from '../addemployee/addemployee';
import { ContactPage } from '../contact/contact';

/**
 * Generated class for the UserSecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-second',
  templateUrl: 'user-second.html',
})
export class UserSecondPage {
  tab1Root = HomePage;
  tab2Root = AddemployeePage;
  tab3Root = ContactPage;
  constructor() {
  }


}
