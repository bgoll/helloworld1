import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HometowPage } from './hometow';

@NgModule({
  declarations: [
    HometowPage,
  ],
  imports: [
    IonicPageModule.forChild(HometowPage),
  ],
})
export class HometowPageModule {}
