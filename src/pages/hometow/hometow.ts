import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AdddirecteurPage } from '../adddirecteur/adddirecteur';
import { AngularFireDatabase ,AngularFireList  } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import firebase from 'firebase' ;
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';


/**
 * Generated class for the HometowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-hometow',
  templateUrl: 'hometow.html',
})
export class HometowPage {
  item1: AngularFireList<any>;
  adddirecteurs: Observable<any[]>;
  public countryList:Array<any>;
public loadedCountryList:Array<any>;
public countryRef:firebase.database.Reference;
public totale:number;
public nm:number ;
 
  private afAuth: AngularFireAuth
 imageSource  ;
  employeePhoto;



  constructor( private alertCtrl: AlertController,public navCtrl: NavController, af: AngularFireDatabase) {
   this.item1 =  af.list('/adddirecteur')

    
    this.adddirecteurs = this.item1.snapshotChanges();

    console.log(this.adddirecteurs);
   

  }


  getPhotoURL(image){
    firebase.storage().ref().child('images/'+ image+'.jpg').getDownloadURL().then((url)=>{
    this.employeePhoto = url;
 
   })
 }
 
 deleteEmployee(key: string)
 {
   
 
 

 
 
    this.item1.remove(key) ;
   
   }
 editPerson(key,login,password) {
     {
   console.log( key, login,  password);
    this.navCtrl.push(AdddirecteurPage,{
    key : key, 
   login : login,
    password : password,
        });
       
       }
 
 }  
 
 
 
 

 }
 