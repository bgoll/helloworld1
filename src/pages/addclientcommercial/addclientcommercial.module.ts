import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddclientcommercialPage } from './addclientcommercial';

@NgModule({
  declarations: [
    AddclientcommercialPage,
  ],
  imports: [
    IonicPageModule.forChild(AddclientcommercialPage),
  ],
})
export class AddclientcommercialPageModule {}
