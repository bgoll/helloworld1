import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HomePage } from '../home/home';
import * as firebase from 'firebase' ;
import { AngularFireDatabase , AngularFireList } from 'angularfire2/database';
import { HomeclientcommercialPage } from '../homeclientcommercial/homeclientcommercial';
import * as moment from 'moment'; 
@Component({
  selector: 'page-addclientcommercial',
  templateUrl: 'addclientcommercial.html',
})
export class AddclientcommercialPage {

  addcommercialsclient: AngularFireList<any>;
  histo: AngularFireList<any>;

  selectedPhoto;
  loading;
  currentImage;
  imageName;
public total:number
  public x:number;
  
    constructor(public navCtrl: NavController,
      public navParams: NavParams,
    public af : AngularFireDatabase,
  public loadingCtrl:LoadingController
  
   ) {
   this.addcommercialsclient = af.list('/addcommercialsclient');
   this.x=Object.keys(this.addcommercialsclient).length ;
   console.log(this.x);
   this.histo = af.list('/histo');
     }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad AddemployeePage');
    }
  
  
  
  
    
  
    createEmployee(name,lname,age,dept){
      this.imageName = name;
   
     
  this.upload()
      this.addcommercialsclient.push({
  
        name : name,
        lname :lname,
        age: age,
        dept:dept,
        image :this.imageName
  
      }).then(newEmployee => {
        this.navCtrl.push(HomeclientcommercialPage);
      })
      this.histo.push({
  
        name : name,
        lname :lname,
        age: age,
        dept:dept,
        date: moment().format("YYYY-MM-DD HH:mm:ss")
      
  
      })
  
  
  }
  
  
  
  
  upload(){
    if(this.selectedPhoto){
      var uploadTask =  firebase.storage().ref().child('images/'+this.imageName+'.jpg').put(this.selectedPhoto);
      uploadTask.then(this.onError);
    }
  }
  
  onError = (error) => {
    console.log(error);
    this.loading.dismiss();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  }
  
