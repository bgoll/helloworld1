import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddclientcommPage } from './addclientcomm';

@NgModule({
  declarations: [
    AddclientcommPage,
  ],
  imports: [
    IonicPageModule.forChild(AddclientcommPage),
  ],
})
export class AddclientcommPageModule {}
