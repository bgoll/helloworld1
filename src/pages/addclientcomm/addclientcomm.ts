import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AngularFireDatabase , AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { HomePage } from '../home/home';
import * as firebase from 'firebase' ;
import { HomeuserPage } from '../homeuser/homeuser';
import * as moment from 'moment'; 
@Component({
  selector: 'page-addclientcomm',
  templateUrl: 'addclientcomm.html',
})
export class AddclientcommPage {
  addclients: AngularFireList<any>;
  histodirecteur: AngularFireList<any>;
  selectedPhoto;
  loading;
  currentImage;
  imageName;
  
  
    constructor(public navCtrl: NavController,
      public navParams: NavParams,
    public af : AngularFireDatabase,
  public loadingCtrl:LoadingController,
  
   ) {
   this.addclients = af.list('/addclients');
   this.histodirecteur = af.list('/histodirecteur');
     }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad AddemployeePage');
    }
    createEmployee(name,lname,age,dept){
      this.imageName = name;
  this.upload()
      this.addclients.push({
  
        name : name,
        lname :lname,
        age: age,
        dept:dept,
        image :this.imageName
  
      }).then(newEmployee => {
        this.navCtrl.push(HomeuserPage);
      })
      this.histodirecteur.push({
  
        name : name,
        lname :lname,
        age: age,
        dept:dept,
        date: moment().format("YYYY-MM-DD HH:mm:ss")
      
  
      })
  
  }
  
  
  
  
  upload(){
    if(this.selectedPhoto){
      var uploadTask =  firebase.storage().ref().child('images/'+this.imageName+'.jpg').put(this.selectedPhoto);
      uploadTask.then(this.onError);
    }
  }
  
  onError = (error) => {
    console.log(error);
    this.loading.dismiss();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  }
  