import { NgModule } from '@angular/core';
import { IonicPageModule, IonicPage } from 'ionic-angular';
import { contactadminPage } from './contactadmin';

@NgModule({
  declarations: [
    contactadminPage,
  ],
  imports: [
    IonicPageModule.forChild(contactadminPage),
  ],
})
export class ContactamdinPageModule {}
