import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireDatabase ,AngularFireList  } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import firebase from 'firebase' ;
import { AddemployeePage } from '../addemployee/addemployee';
import { AddclientcommPage } from '../addclientcomm/addclientcomm';

/**
 * Generated class for the HomeuserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-homeuser',
  templateUrl: 'homeuser.html',
})
export class HomeuserPage {
  item3: AngularFireList<any>;
  addclients: Observable<any[]>;
  public totale:number;
  public nm:number ;

 imageSource  ;
  employeePhoto;



  constructor(private alertCtrl: AlertController,public navCtrl: NavController, af: AngularFireDatabase) {
   this.item3 =  af.list('/addclients')

    
    this.addclients = this.item3.snapshotChanges();

    console.log(this.addclients);

  }


getPhotoURL(image){
   firebase.storage().ref().child('images/'+ image+'.jpg').getDownloadURL().then((url)=>{
   this.employeePhoto = url;

  })
}




public idget1
public updateget1
deleteEmployee(key: string)
{
  

 firebase.database().ref('/iddiecteurdelete/').on('child_added', (data) =>{this.idget1=data.val().id
 
  console.log(this.idget1)

  });
 if(this.idget1==1)
 {
     let alert = this.alertCtrl.create({
    title: 'blocked from admin',
    subTitle: 'sorry you cant delete',
    buttons: ['Dismiss']
     });
  alert.present();   
    }
    else
  



   this.item3.remove(key) ;
  
  }
editPerson(key,login,password) {

  firebase.database().ref('/iddirecteurupdate/').on('child_added', (data) =>{this.updateget1=data.val().id
 
    console.log(this.updateget1)
  
    });
    if (this.updateget1==1)
    {
      let alert = this.alertCtrl.create({
     title: 'blocked from admin',
     subTitle: 'sorry you cant update',
     buttons: ['Dismiss']
      });
   alert.present();   
     }
    else
    {
  console.log( key, login,  password);
   this.navCtrl.push(AddclientcommPage ,{
   key : key, 
  login : login,
   password : password,
       });
      
      }

}  






      

  addThemFunction (totale) {
    this.addclients.subscribe(result => {
     console.log(result.length)   

 this.totale = 131+result.length  ;
 
});
  
  }
     

   
}
