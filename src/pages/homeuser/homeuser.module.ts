import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeuserPage } from './homeuser';

@NgModule({
  declarations: [
    HomeuserPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeuserPage),
  ],
})
export class HomeuserPageModule {}
