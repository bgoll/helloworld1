import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , LoadingController} from 'ionic-angular';
import { AngularFireDatabase , AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { HomePage } from '../home/home';
import * as firebase from 'firebase' ;

import * as moment from 'moment'; 




/**
 * Generated class for the AddemployeePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addemployee',
  templateUrl: 'addemployee.html',
})
export class AddemployeePage {

  employess: AngularFireList<any>;
  historiquetechnoco:AngularFireList<any>;
selectedPhoto;
loading;
currentImage;
imageName;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
  public af : AngularFireDatabase,
public loadingCtrl:LoadingController,

 ) {
 this.employess = af.list('/employees');
 this.historiquetechnoco = af.list('/historiquetechnoco');
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddemployeePage');
  }










  createEmployee(name,lname,age,dept){
    this.imageName = name;
this.upload()
    this.employess.push({

      name : name,
      lname :lname,
      age: age,
      dept:dept,
      image :this.imageName

    }).then(newEmployee => {
      this.navCtrl.push(HomePage);
    })
    this.historiquetechnoco.push({
  
      name : name,
      lname :lname,
      age: age,
      dept:dept,
      date: moment().format("YYYY-MM-DD HH:mm:ss")
    

    })

}




upload(){
  if(this.selectedPhoto){
    var uploadTask =  firebase.storage().ref().child('images/'+this.imageName+'.jpg').put(this.selectedPhoto);
    uploadTask.then(this.onError);
  }
}

onError = (error) => {
  console.log(error);
  this.loading.dismiss();
}















}
