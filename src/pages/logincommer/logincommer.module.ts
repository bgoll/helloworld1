import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogincommerPage } from './logincommer';

@NgModule({
  declarations: [
    LogincommerPage,
  ],
  imports: [
    IonicPageModule.forChild(LogincommerPage),
  ],
})
export class LogincommerPageModule {}
