import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AddclientcommercialPage } from '../addclientcommercial/addclientcommercial';
import { AngularFireAuth } from 'angularfire2/auth';

import { Observable } from 'rxjs/Observable';

import { AngularFireList  } from 'angularfire2/database';

@IonicPage()
@Component({
  selector: 'page-logincommer',
  templateUrl: 'logincommer.html',
})
export class LogincommerPage {
  item4: AngularFireList<any>;
  commmercial: Observable<any[]>; 
email:string = '';
password:string = '';

constructor(public navCtrl: NavController, public navParams: NavParams ,public fire:AngularFireAuth) {
  }

myLogIn(){
    
    this.fire.auth.signInWithEmailAndPassword(this.email, this.password).then(user=>{
            console.log(this.email +"  "+this.password)
    this.navCtrl.push(AddclientcommercialPage)
        }).catch(function(error) {
  // Handle Errors here.
   console.log(error);
  // ...
});  
    
    
    console.log("email:"+this.email+"password:"+ this.password)
}

}
