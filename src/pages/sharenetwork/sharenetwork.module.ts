import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharenetworkPage } from './sharenetwork';

@NgModule({
  declarations: [
    SharenetworkPage,
  ],
  imports: [
    IonicPageModule.forChild(SharenetworkPage),
  ],
})
export class SharenetworkPageModule {}
