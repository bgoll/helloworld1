import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Http } from '@angular/http'; // don't forget to import HttpModule in app.module.ts
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';
import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';
import { debutPage } from '../debut/debut';
/**
 * Generated class for the SharenetworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sharenetwork',
  templateUrl: 'sharenetwork.html',
})
export class SharenetworkPage {
  user: any;
  FB_APP_ID: number = 725261520963213;
  userReady: boolean = false;
  public sendTo   : any;
  public subject  : string = 'Message from Social Sharing App';
  public message  : string = 'Take your app development skills to the next level with Mastering Ionic - the definitive guide';
  public image    : string	= 'http://masteringionic2.com/perch/resources/mastering-ionic-2-cover-1-w320.png';
  public uri      : string	= 'http://masteringionic2.com/products/product-detail/s/mastering-ionic-2-e-book';


  quotes :any;
  private  apiUrl :string = "http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=10"; //api url to retrieve 10 random quotes
  constructor(  public nativeStorage: NativeStorage,  public fb: Facebook, private file: File, 
    private fileOpener: FileOpener, 
   public platform   : Platform,private http:Http, private socialSharing: SocialSharing ,public navCtrl: NavController) {
    this.getQuotes();
    
 

  }
  async getQuotes(){
          this.quotes = await this.http.get(this.apiUrl).map(res => res.json()).toPromise();;
}
compilemsg(index):string{
  var msg = this.quotes[index].content + "-" + this.quotes[index].title ;
  return msg.concat(" \n Sent from my Awesome App !");
}

regularShare(index){
  var msg = this.compilemsg(index);
  this.socialSharing.share(msg, null, null, null);
}
whatsappShare(index){
  var msg  = this.compilemsg(index);
   this.socialSharing.shareViaWhatsApp(msg, null, null);
}
twitterShare(index){
  var msg  = this.compilemsg(index);
  this.socialSharing.shareViaTwitter(msg, null, null);
}
facebookShare(index){
  var msg  = this.compilemsg(index);
   this.socialSharing.shareViaFacebook(msg, null, null);
}
doRefresh(refresher) {
  this.getQuotes();  // calls the getQuotes method
  setTimeout(() => {
    refresher.complete(); // stops the refresher 2 seconds after retreiving the Data
  }, 2000);
}



shareViaEmail()
{
   this.platform.ready()
   .then(() =>
   {
      this.socialSharing.canShareViaEmail()
      .then(() =>
      {
         this.socialSharing.shareViaEmail(this.message, this.subject, this.sendTo)
         .then((data) =>
         {
            console.log('Shared via Email');
         })
         .catch((err) =>
         {
            console.log('Not able to be shared via Email');
         });
      })
      .catch((err) =>
      {
         console.log('Sharing via Email NOT enabled');
      });
   });
}



shareViaFacebook()
{
   this.platform.ready()
   .then(() =>
   {
      this.socialSharing.canShareVia('com.apple.social.facebook', this.message, this.image, this.uri)
      .then((data) =>
      {

         this.socialSharing.shareViaFacebook(this.message, this.image, this.uri)
         .then((data) =>
         {
            console.log('Shared via Facebook');
         })
         .catch((err) =>
         {
            console.log('Was not shared via Facebook');
         });

      })
      .catch((err) =>
      {
         console.log('Not able to be shared via Facebook');
      });

   });
}




shareViaInstagram()
{
   this.platform.ready()
   .then(() =>
   {

      this.socialSharing.shareViaInstagram(this.message, this.image)
      .then((data) =>
      {
         console.log('Shared via shareViaInstagram');
      })
      .catch((err) =>
      {
         console.log('Was not shared via Instagram');
      });

   });
}




sharePicker()
{
   this.platform.ready()
   .then(() =>
   {

      this.socialSharing.share(this.message, this.subject, this.image, this.uri)
      .then((data) =>
      {
         console.log('Shared via SharePicker');
      })
      .catch((err) =>
      {
         console.log('Was not shared via SharePicker');
      });

   });
}




shareViaTwitter()
{
   this.platform.ready()
   .then(() =>
   {

      this.socialSharing.canShareVia('com.apple.social.twitter', this.message, this.image, this.uri)
      .then((data) =>
      {

         this.socialSharing.shareViaTwitter(this.message, this.image, this.uri)
         .then((data) =>
         {
            console.log('Shared via Twitter');
         })
         .catch((err) =>
         {
            console.log('Was not shared via Twitter');
         });

      });

   })
   .catch((err) =>
   {
      console.log('Not able to be shared via Twitter');
   });
  }
    


  //Save Image Function
  saveImg() {
    let imageName = "FreakyJolly.jpg";
    const ROOT_DIRECTORY = 'file:///sdcard//';
    const downloadFolderName = 'tempDownloadFolder';
    
    //Create a folder in memory location
    this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
      .then((entries) => {

        //Copy our asset/img/FreakyJolly.jpg to folder we created
        this.file.copyFile(this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
          .then((entries) => {

            //Open copied file in device's default viewer
            this.fileOpener.open(ROOT_DIRECTORY + downloadFolderName + "/" + imageName, 'image/jpeg')
              .then(() => console.log('File is opened'))
              .catch(e => alert('Error' + JSON.stringify(e)));
          })
          .catch((error) => {
            alert('error ' + JSON.stringify(error));
          });
      })
      .catch((error) => {
        alert('error' + JSON.stringify(error));
      });
  }

  shareImg() { 
    let imageName = "FreakyJolly.jpg";
    const ROOT_DIRECTORY = 'file:///sdcard//';
    const downloadFolderName = 'tempDownloadFolder';
    
    //Create a folder in memory location
    this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
      .then((entries) => {

        //Copy our asset/img/FreakyJolly.jpg to folder we created
        this.file.copyFile(this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
          .then((entries) => {

            //Common sharing event will open all available application to share
            this.socialSharing.share("Message","Subject", ROOT_DIRECTORY + downloadFolderName + "/" + imageName, imageName)
              .then((entries) => {
                console.log('success ' + JSON.stringify(entries));
              })
              .catch((error) => {
                alert('error ' + JSON.stringify(error));
              });
          })
          .catch((error) => {
            alert('error ' + JSON.stringify(error));
          });
      })
      .catch((error) => {
        alert('error ' + JSON.stringify(error));
      });
  }


  ionViewCanEnter(){
    this.nativeStorage.getItem('user')
    .then((data) => {
      this.user = {
        name: data.name,
        gender: data.gender,
        picture: data.picture
      };
      this.userReady = true;
    }, (error) => {
      console.log(error);
    });
  }

  doFbLogout(){
    var nav = this.navCtrl;
    this.fb.logout()
    .then((response) => {
      //user logged out so we will remove him from the NativeStorage
      this.nativeStorage.remove('user');
      nav.push(debutPage);
    }, (error) => {
      console.log(error);
    });
  }
  doFbLogin(){
    let permissions = new Array<string>();
    let nav = this.navCtrl;

    //the permissions your facebook app needs from the user
    permissions = ["public_profile"];

    this.fb.login(permissions)
    .then((response) => {
      let userId = response.authResponse.userID;
      let params = new Array<string>();

      //Getting name and gender properties
      this.fb.api("/me?fields=name,gender", params)
      .then((user) => {
        user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
        //now we have the users info, let's save it in the NativeStorage
        this.nativeStorage.setItem('user',
        {
          name: user.name,
          gender: user.gender,
          picture: user.picture
        })
        .then(() => {
          nav.push(debutPage);
        },(error) => {
          console.log(error);
        })
      })
    }, (error) => {
      console.log(error);
    });
  }

}

