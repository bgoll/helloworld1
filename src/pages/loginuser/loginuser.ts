import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

import { AngularFireAuth } from 'angularfire2/auth';

import { Observable } from 'rxjs/Observable';
import firebase from 'firebase' ;
import { AddemployeePage } from '../addemployee/addemployee';
import { AngularFireDatabase ,AngularFireList  } from 'angularfire2/database';
import { HomeuserPage } from '../homeuser/homeuser';
import { AddclientcommPage } from '../addclientcomm/addclientcomm';
/**
 * Generated class for the LoginuserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loginuser',
  templateUrl: 'loginuser.html',
})
export class LoginuserPage {
  item: AngularFireList<any>;
  directeur: Observable<any[]>; 
email:string = '';
password:string = '';

constructor(public navCtrl: NavController, public navParams: NavParams ,public fire:AngularFireAuth) {
  }

myLogIn(){
    
    this.fire.auth.signInWithEmailAndPassword(this.email, this.password).then(user=>{
            console.log(this.email +"  "+this.password)
    this.navCtrl.push(AddclientcommPage)
        }).catch(function(error) {
  // Handle Errors here.
   console.log(error);
  // ...
});  
    
    
    console.log("email:"+this.email+"password:"+ this.password)
}

}
