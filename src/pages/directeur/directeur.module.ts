import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirecteurPage } from './directeur';

@NgModule({
  declarations: [
    DirecteurPage,
  ],
  imports: [
    IonicPageModule.forChild(DirecteurPage),
  ],
})
export class DirecteurPageModule {}
