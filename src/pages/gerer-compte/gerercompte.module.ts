import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminSecondPage } from './gerercompte';

@NgModule({
  declarations: [
    AdminSecondPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminSecondPage),
  ],
})
export class AdminSecondPageModule {}
