import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { HometowPage } from '../hometow/hometow';
import { AdddirecteurPage } from '../adddirecteur/adddirecteur';
import { AddcomercialPage } from '../addcomercial/addcomercial';


@IonicPage()
@Component({
  selector: 'page-gerercompte',
  templateUrl: 'gerercompte.html',
})
export class AdminSecondPage {
 
email:string = '';
password : string = '';
 myError='';   
    
constructor(public fire:AngularFireAuth ,public navCtrl: NavController, public navParams: NavParams) {
  }


myRegister(){
    
    this.fire.auth.createUserWithEmailAndPassword(this.email, this.password).then(user=>{
            console.log(this.email +"  "+this.password);
        
      this.myError = this.email +"  "+this.password;
        
        
        }).catch(function(error) {
  // Handle Errors here.
   console.log(error);
            
 
             
  // ...
});  
    
this.navCtrl.push(AddcomercialPage)    
    

}
    
  
}