import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdddirecteurPage } from './adddirecteur';

@NgModule({
  declarations: [
    AdddirecteurPage,
  ],
  imports: [
    IonicPageModule.forChild(AdddirecteurPage),
  ],
})
export class AdddirecteurPageModule {}
