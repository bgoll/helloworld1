import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AngularFireDatabase , AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

import * as firebase from 'firebase' ;
import { HometowPage } from '../hometow/hometow';
import { HomathreePage } from '../homathree/homathree';
/**
 * Generated class for the AdddirecteurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-adddirecteur',
  templateUrl: 'adddirecteur.html',
})
export class AdddirecteurPage {

  adddirecteurs: AngularFireList<any>;

  selectedPhoto;
  loading;
  currentImage;
  imageName;
  
  
    constructor(public navCtrl: NavController,
      public navParams: NavParams,
    public af : AngularFireDatabase,
  public loadingCtrl:LoadingController,
  
   ) {
   this.adddirecteurs = af.list('/adddirecteur');
     }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad AddemployeePage');
    }
  
  
  

    createdirecteur(login,password){
      this.imageName = name;
  this.upload()
      this.adddirecteurs.push({
        login : login,
        password :password 
  
      }).then(newEmployee => {
        this.navCtrl.push(HometowPage);
      })
  
  
  }
  
  
  
  upload(){
    if(this.selectedPhoto){
      var uploadTask =  firebase.storage().ref().child('images/'+this.imageName+'.jpg').put(this.selectedPhoto);
      uploadTask.then(this.onError);
    }
  }
  
  onError = (error) => {
    console.log(error);
    this.loading.dismiss();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  }
  