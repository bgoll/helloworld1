import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import {AlertController } from 'ionic-angular';
/**
 * Generated class for the DebutloginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-debutlogin',
  templateUrl: 'debutlogin.html',
})
export class DebutloginPage {
  user = {
    name: 'admin',
    pw: 'admin'
  };

  constructor(public navCtrl: NavController, private authProvider: AuthProvider, private alertCtrl: AlertController) { }

  loginUser() {
    this.authProvider.login(this.user.name, this.user.pw).then(success => {
      if (success) {
        this.navCtrl.setRoot('MenuPage');
      } else {
        let alert = this.alertCtrl.create({
          title: 'Login failed',
          message: 'Please check your credentials',
          buttons: ['OK']
        });
        alert.present();
      }
    });
  }
}