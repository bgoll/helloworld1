import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DebutloginPage } from './debutlogin';

@NgModule({
  declarations: [
    DebutloginPage,
  ],
  imports: [
    IonicPageModule.forChild(DebutloginPage),
  ],
})
export class DebutloginPageModule {}
