import { AuthProvider } from '../../providers/auth/auth';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, App } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { LocalNotifications } from '@ionic-native/local-notifications';
@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  rootPage: any;
  pages = [];
  username = '';

  // Reference to the side menus root nav
  @ViewChild(Nav) nav: Nav;
  lang:any;
  constructor(public navCtrl: NavController, private authProvider: AuthProvider, private appCtrl: App,
    public translate: TranslateService)
   {    
    this.lang = 'en';
    this.translate.setDefaultLang('en');
    this.translate.use('en');
  }
    
  switchLanguage() {
    this.translate.use(this.lang);
   }

    // switchLanguage() {
    // if(   (this.lang = 'ar') ||
   //(this.translate.setDefaultLang('ar'))
    // ||(this.translate.use('ar') ))
    // {
    //  if (this.authProvider.isAdmin()) {
       // this.pages = [
        //  { title: 'حساب مشرف المبيعات الفنية', page: 'contactadminPage', icon: 'home' },
       // { title: 'حساب مدير  مشرف', page: 'EditPersonPage', icon: 'planet' },
       // { title: ' الفنية مشرفة', page: 'AdminSecondPage', icon: 'planet' },
        //  { title: 'ارسل بريد الكتروني', page: 'ContactusPage', icon: 'home' },
         // { title: 'أرسل رسالة نصية قصيرة', page: 'AdminSecondPage', icon: 'planet' }
       // ];
       // this.openPage('AdminPage');
     // } else {
      //  this.pages = [
        //  { title: 'معلومات المستخدم ', page: 'UserPage', icon: 'home' },
         // { title: 'مبيعات تقنية', page: 'LoginPage', icon: 'planet' },
         // { title: 'مدير', page: 'LoginuserPage', icon: 'planet' },
         // { title: 'تجاري', page: 'LogincommerPage', icon: 'planet' }
      //  ];
      //  this.openPage('UserPage');
    
 
    //  } } else  if( (this.lang = 'en') 
     // ||(this.translate.use('en') ))
     // {

    // if (this.authProvider. issup()) {
        //  this.pages = [
         //   { title: 'permission for  Technical Sales', page: 'TechnococommercialPage', icon: 'home' },
         // { title: 'permission for director', page: 'DirecteurPage', icon: 'planet' },
         // { title: 'permission for commercial ', page: 'PermissionPage', icon: 'planet' },
  
       //   ];
  
       
      //    this.openPage('PermissionPage');        }
    //  { if (this.authProvider.isAdmin()) {
      //  this.pages = [
        //  { title: 'Admin  account Technical Sales', page: 'contactadminPage', icon: 'home' },
      //  { title: 'Admin account director', page: 'EditPersonPage', icon: 'planet' },
       // { title: 'Admin commercial accounte', page: 'AdminSecondPage', icon: 'planet' },
         //  { title: 'send email', page: 'ContactusPage', icon: 'home' },
         // { title: 'send sms', page: 'AdminSecondPage', icon: 'planet' }
      //  ];
       // this.openPage('AdminPage');
    //  }
 
    
     //  else {
       // this.pages = [
        //  { title: 'User  Information ', page: 'UserPage', icon: 'home' },
        //  { title: 'technical sales ', page: 'LoginPage', icon: 'planet' },
        //  { title: 'director', page: 'LoginuserPage', icon: 'planet' },
         // { title: 'commercial', page: 'LogincommerPage', icon: 'planet' }
      //  ];
      //  this.openPage('UserPage');
     // }
    //  this.username = this.authProvider.currentUser.name;
  
  //  }
 // }
 // }

  ionViewWillEnter() {
     if (this.authProvider. issup()) {
      this.pages = [
        { title: 'permission for  Technical Sales', page: 'TechnococommercialPage', icon: 'home' },
      { title: 'permission for director', page: 'DirecteurPage', icon: 'planet' },
      { title: 'permission for commercial ', page: 'PermissionPage', icon: 'planet' },

      ];

     
      this.openPage('PermissionPage');        }
    
    if (this.authProvider.isAdmin()) {
      this.pages = [
        { title: 'Admin  account Technical Sales', page: 'contactadminPage', icon: 'home' },
      { title: 'Admin account director', page: 'EditPersonPage', icon: 'planet' },
      { title: 'Admin commercial accounte', page: 'AdminSecondPage', icon: 'planet' },
        { title: 'send email', page: 'ContactusPage', icon: 'home' },
        { title: 'evaluation', page: 'ChartsPage', icon: 'planet,' },
  { title: 'Add client', page: 'AddNotePage', icon: 'planet' },
  { title: 'historic', page: 'HistoricPage', icon: 'planet' },
  { title: 'share in social media', page: 'SharenetworkPage', icon: 'planet' }
 

  

      ];
      this.openPage('AdminPage');
    } 
    
    else  if (this.authProvider. issup()) {
      this.pages = [
        { title: 'permission for  Technical Sales', page: 'TechnococommercialPage', icon: 'home' },
      { title: 'permission for director', page: 'DirecteurPage', icon: 'planet' },
      { title: 'permission for commercial ', page: 'PermissionPage', icon: 'planet' },

      ];

     
      this.openPage('PermissionPage');        }
    
    
    else {
      this.pages = [
        { title: 'User  Information ', page: 'UserPage', icon: 'home' },
        { title: 'technical sales ', page: 'LoginPage', icon: 'planet' },
        { title: 'director', page: 'LoginuserPage', icon: 'planet' },
        { title: 'commercial', page: 'LogincommerPage', icon: 'planet' }
      ];
      this.openPage('UserPage');
    }
    this.username = this.authProvider.currentUser.name;
  }
   
 
  logout() {
    this.authProvider.logout();
    this.appCtrl.getRootNav().setRoot('DebutloginPage');
  }

  openPage(page) {
    this.nav.setRoot(page);
  }

  ionViewCanEnter() {
    return this.authProvider.isLoggedIn();
  }

}