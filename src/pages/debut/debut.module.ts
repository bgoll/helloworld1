import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { debutPage } from './debut';

@NgModule({
  declarations: [
    debutPage,
  ],
  imports: [
    IonicPageModule.forChild(debutPage),
  ],
})
export class debutPageModule {}
