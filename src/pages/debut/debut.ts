import { AuthProvider } from '../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { LanguageService } from "../../providers/auth/language.service";
import { LanguageModel } from "../../models/language.model";
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-debut',
  templateUrl: 'debut.html',
})
export class debutPage {
  user = {
    name: 'admin',
    pw: 'admin'
  };
  lang:any;
  languageSelected : any = 'en';
  languages : Array<LanguageModel>;
 

  constructor(public translate: TranslateService, public ga: GoogleAnalytics,public navCtrl: NavController, private authProvider: AuthProvider, private alertCtrl: AlertController) { 

    this.ga.startTrackerWithId('UA-123951015-1')
    .then(() => {
      console.log('Google analytics is ready now');
      this.ga.trackView('home');
    })
    .catch(e => console.log('Error starting GoogleAnalytics', e));
   

    this.lang = 'en';
    this.translate.setDefaultLang('en');
    this.translate.use('en');
}
switchLanguage() {
 this.translate.use(this.lang);
}
  loginUser() {
    this.authProvider.login(this.user.name, this.user.pw).then(success => {
      if (success) {
        this.navCtrl.setRoot('MenuPage');
      } else {
        let alert = this.alertCtrl.create({
          title: 'Login failed',
          message: 'Please check your credentials',
          buttons: ['OK']
        });
        alert.present();
      }
    });
  }
}