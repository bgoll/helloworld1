import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomathreePage } from './homathree';

@NgModule({
  declarations: [
    HomathreePage,
  ],
  imports: [
    IonicPageModule.forChild(HomathreePage),
  ],
})
export class HomathreePageModule {}
