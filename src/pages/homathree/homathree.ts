import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdddirecteurPage } from '../adddirecteur/adddirecteur';
import { AngularFireDatabase ,AngularFireList  } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import firebase from 'firebase' ;
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';
import { AddcomercialPage } from '../addcomercial/addcomercial';
/**
 * Generated class for the HomathreePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-homathree',
  templateUrl: 'homathree.html',
})
export class HomathreePage {
  item2: AngularFireList<any>;
  employees: Observable<any[]>;
  addcomercials: Observable<any[]>;

 imageSource  ;
  employeePhoto;



  constructor(public navCtrl: NavController, af: AngularFireDatabase) {
   this.item2 =  af.list('/addcomercial')

    
    this.addcomercials = this.item2.snapshotChanges();

    console.log(this.addcomercials);

  }







deleteEmployee(key: string)
  {
    this.item2.remove(key);
  }


deleteItem(key: string): void {
  this.item2.remove(key) ;
  
}

editPerson(key,login,password) {
  console.log( key, login,  password);
   this.navCtrl.push(AddcomercialPage,{
   key : key, 
  login : login,
   password : password,

       });
   
}
  
}
