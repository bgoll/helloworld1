import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TechnococommercialPage } from './technococommercial';

@NgModule({
  declarations: [
    TechnococommercialPage,
  ],
  imports: [
    IonicPageModule.forChild(TechnococommercialPage),
  ],
})
export class TechnococommercialPageModule {}
