import { Component } from '@angular/core';

import { ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AngularFireList } from 'angularfire2/database/interfaces';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Chart } from 'chart.js';
import { AdddirecteurPage } from '../adddirecteur/adddirecteur';
import { AddcomercialPage } from '../addcomercial/addcomercial';
import { AddemployeePage } from '../addemployee/addemployee';

@IonicPage()
@Component({
  selector: 'page-charts',
  templateUrl: 'charts.html',
})
export class ChartsPage {
  item4: AngularFireList<any>;
  item5: AngularFireList<any>;
public totale1:number ;
  public nm:number
  item3: AngularFireList<any>;
  addclients: Observable<any[]>;
  item: AngularFireList<any>;
  employees: Observable<any[]>;
  public totale2:number 
  addcommercialsclient: Observable<any[]>;
  public totale:number
  item1: AngularFireList<any>;
  adddirecteurs: Observable<any[]>;
  item2: AngularFireList<any>;
public totaleval:number ;
  addcomercials: Observable<any[]>;
  data: Observable<any[]>;
  ref: AngularFireList<any>;
  @ViewChild('lineCanvas') lineCanvas;

 
  months = [
    {value: 0, name: 'January'},
    {value: 1, name: 'February'},
    {value: 2, name: 'March'},
    {value: 3, name: 'April'},
  ];
 
  transaction = {
    value: 0,
    expense: false,
    month: 0
  }
 
  @ViewChild('valueBarsCanvas') valueBarsCanvas;
  valueBarsChart: any;
 
  chartData = null;
  chart = []; 
  arraytoPassChart = []
  constructor(public navCtrl: NavController, private af: AngularFireDatabase, private toastCtrl: ToastController) {
    this.item1 =  af.list('/adddirecteur')
    this.adddirecteurs = this.item1.snapshotChanges();
    this.item4 =  af.list('/addcommercialsclient');
    this.item2 =  af.list('/addcomercial')
    this.item =  af.list('/employees')

    this.item3 =  af.list('/addclients')

    
    this.addclients = this.item3.snapshotChanges();
    this.employees = this.item.snapshotChanges();

    
    this.addcomercials = this.item2.snapshotChanges();
  this.nm=1 ;
    this.addcommercialsclient= this.item4.snapshotChanges();
  
  
  }
  makeChart() {
    this.chart = new Chart(this.lineCanvas.nativeElement, {

      type: 'line',
      data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
          {
            label: "My First dataset",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.arraytoPassChart,// my data here, work when I type an array myself
            spanGaps: false,
          }
        ]
      }

    });
  }
  ionViewDidLoad() {
 

    // Reference to our Firebase List
    this.ref = this.af.list('transactions', ref => ref.orderByChild('month'));
 
    // Catch any update to draw the Chart
    this.ref.valueChanges().subscribe(result => {
      if (this.chartData) {
        this.updateCharts(result)
      } else {
        this.createCharts(result)
      }
    })
  }
  addTransaction() {
    this.ref.push(this.transaction).then(() => {
      this.transaction = {
        value: 0,
        month: 0,
        expense: false
      };
      let toast = this.toastCtrl.create({
        message: 'New Transaction added',
        duration: 3000
      });
      toast.present();
    })
}

getReportValues() {
  let reportByMonth = {
    0: null,
    1: null,
    2: null,
    3: null
  };
 
  for (let trans of this.chartData) {
    if (reportByMonth[trans.month]) {
      if (trans.expense) {
        reportByMonth[trans.month] -= +trans.value;
      } else {
        reportByMonth[trans.month] += +trans.value;
      }
    } else {
      if (trans.expense) {
        reportByMonth[trans.month] = 0 - +trans.value;
      } else {
        reportByMonth[trans.month] = +trans.value;
      }
    }
  }
  return Object.keys(reportByMonth).map(a => reportByMonth[a]);
}
createCharts(data) {
  this.chartData = data;
 
  // Calculate Values for the Chart
  let chartData = this.getReportValues();
 
  // Create the chart
  this.valueBarsChart = new Chart(this.valueBarsCanvas.nativeElement, {
    type: 'bar',
    data: {
      labels: Object.keys(this.months).map(a => this.months[a].name),
      datasets: [{
        data: chartData,
        backgroundColor: '#32db64'
      }]
    },
    options: {
      legend: {
        display: false
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItems, data) {
            return data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index] +' dt';
          }
        }
      },
      scales: {
        xAxes: [{
          ticks: {
            beginAtZero: true
          }
        }],
        yAxes: [{
          ticks: {
            callback: function (value, index, values) {
              return value + 'dt';
            },

            suggestedMin: 0
          }
        }]
      },
    }
  });
}

updateCharts(data) {
  this.chartData = data;
  let chartData = this.getReportValues();
 
  // Update our dataset
  this.valueBarsChart.data.datasets.forEach((dataset) => {
    dataset.data = chartData
  });
  this.valueBarsChart.update();
}

  
deleteEmployee(key: string)
{
  





   this.item1.remove(key) ;
  
  }
editPerson(key,login,password) {
    {
  console.log( key, login,  password);
   this.navCtrl.push(AdddirecteurPage,{
   key : key, 
  login : login,
   password : password,
       });
      
      }

}  
addThemFunction (totale) {
  this.addcommercialsclient.subscribe(result => {
   console.log(result.length)   

this.totale = this.nm+131+result.length  ;



  
});

}
public rating:any
changeRating(event)
{
   this.rating=event.value;
}

deleteEmployee1(key: string)
  {
    this.item2.remove(key);
  }


deleteItem(key: string): void {
  this.item2.remove(key) ;
  
}

editPerson1(key,login,password) {
  console.log( key, login,  password);
   this.navCtrl.push(AddcomercialPage,{
   key : key, 
  login : login,
   password : password,

       });
   
}
addThemFunction1 () {
  this.addclients.subscribe(result => {
   console.log(result.length)   

this.totale1 = 131+result.length  ;

});

}

deleteEmployee2(key: string)
{
  

   this.item.remove(key) ;
  
  }
editPerson2(key,login,password) {
 

  console.log( key, login,  password);
   this.navCtrl.push(AddemployeePage,{
   key : key, 
  login : login,
   password : password,
       });
      
      }
  

    
addThemFunction2 (totale2) {
  this.employees.subscribe(result => {
   console.log(result.length)   

this.totale2 = 170+result.length  ;

});

}

totalevaleur()
{
this.totaleval=this.totale+this.totale1+this.totale2


}
}
