import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChartsPage } from './charts';
import { Ionic2RatingModule } from "ionic2-rating";
@NgModule({
  declarations: [
    ChartsPage,
  ],
  imports: [
    IonicPageModule.forChild(ChartsPage),
    Ionic2RatingModule 
  ],
})
export class ChartsPageModule {}
