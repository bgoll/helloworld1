import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import * as firebase from 'firebase' ;
import { AngularFireDatabase , AngularFireList } from 'angularfire2/database';
import { HomeclientcommercialPage } from '../homeclientcommercial/homeclientcommercial';
import { AddnotehtmlPage } from '../addnotehtml/addnotehtml';



@IonicPage()
@Component({
  selector: 'page-add-note',
  templateUrl: 'add-note.html',
})
export class AddNotePage {

  admin: AngularFireList<any>;

  selectedPhoto;
  loading;
  currentImage;
  imageName;
  
  
public total:number
public x:number;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
  public af : AngularFireDatabase,
public loadingCtrl:LoadingController

 ) {
 this.admin = af.list('/admin');
 this.x=Object.keys(this.admin).length ;
 console.log(this.x);
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddemployeePage');
  }




  

  createEmployee(name,lname,age,dept){
    this.imageName = name;
this.upload()
    this.admin.push({

      name : name,
      lname :lname,
      age: age,
      dept:dept,
      image :this.imageName

    }).then(newEmployee => {
      this.navCtrl.push(AddnotehtmlPage);
    })


}




upload(){
  if(this.selectedPhoto){
    var uploadTask =  firebase.storage().ref().child('images/'+this.imageName+'.jpg').put(this.selectedPhoto);
    uploadTask.then(this.onError);
  }
}

onError = (error) => {
  console.log(error);
  this.loading.dismiss();
}















}

