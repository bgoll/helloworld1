import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddNotePage } from './add-note';
import { AngularFireModule } from 'angularfire2';

 const config = {
    apiKey: "AIzaSyBCglLLgUuCU3BbMY2Ipo04GV2nAGeXPGI",
    authDomain: "mydata-68a83.firebaseapp.com",
    databaseURL: "https://mydata-68a83.firebaseio.com",
    projectId: "mydata-68a83",
    storageBucket: "mydata-68a83.appspot.com",
    messagingSenderId: "990020390983"
  };


@NgModule({
  declarations: [
    AddNotePage,
  ],
  imports: [
    AngularFireModule.initializeApp(config),
    IonicPageModule.forChild(AddNotePage),

  ],
})
export class AddNotePageModule {}
