import { Component } from '@angular/core';
import { NavController, ActionSheetController, AlertController } from 'ionic-angular';
import { AngularFireDatabase ,AngularFireList  } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import firebase from 'firebase' ;
import { AddemployeePage } from '../addemployee/addemployee';
import { PermissionPage } from '../permission/permission';




@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  item: AngularFireList<any>;
  employees: Observable<any[]>;
 public id:string ;
 public totale:number;
public nm:number ;

 imageSource  ;
  employeePhoto;



  constructor(private alertCtrl: AlertController,public navCtrl: NavController, af: AngularFireDatabase) {
   this.item =  af.list('/employees')

    
    this.employees = this.item.snapshotChanges();

    console.log(this.employees);

  }


getPhotoURL(image){
   firebase.storage().ref().child('images/'+ image+'.jpg').getDownloadURL().then((url)=>{
   this.employeePhoto = url;

  })
}


public idget2
public updateget2
deleteEmployee(key: string)
{
  

  



   this.item.remove(key) ;
  
  }
editPerson(key,login,password) {
 

  console.log( key, login,  password);
   this.navCtrl.push(AddemployeePage,{
   key : key, 
  login : login,
   password : password,
       });
      
      }


      addThemFunction (totale) {
        this.employees.subscribe(result => {
         console.log(result.length)   
   
     this.totale = 170+result.length  ;
     
   });
      
      }
    
}  







