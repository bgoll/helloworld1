import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddcomercialPage } from './addcomercial';

@NgModule({
  declarations: [
    AddcomercialPage,
  ],
  imports: [
    IonicPageModule.forChild(AddcomercialPage),
  ],
})
export class AddcomercialPageModule {}
