
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { fireb} from '../../providers/firebase';

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {
    
  user = {
    name: 'superadmin',
    pw: 'superadmin'
  };

  constructor(public navCtrl: NavController, private authProvider: fireb, private alertCtrl: AlertController) { }

  loginUser() {
    this.authProvider.login(this.user.name, this.user.pw).then(success => {
      if (success) {
        this.navCtrl.setRoot('MenuPage');
      } else {
        let alert = this.alertCtrl.create({
          title: 'Login failed',
          message: 'Please check your credentials',
          buttons: ['OK']
        });
        alert.present();
      }
    });
  }
}