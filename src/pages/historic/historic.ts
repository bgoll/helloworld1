import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs'
import { Subscription } from 'rxjs/Subscription'
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/throttleTime';
import {  AngularFireList } from 'angularfire2/database';
import 'rxjs/add/operator/do';
import * as moment from 'moment'; 
import { AngularFirestore } from 'angularfire2/firestore';
import { AddclientcommercialPage } from '../addclientcommercial/addclientcommercial';
import { getNonHydratedSegmentIfLinkAndUrlMatch } from '../../../node_modules/ionic-angular/umd/navigation/url-serializer';


/**
 * Generated class for the HistoricPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-historic',
  templateUrl: 'historic.html',
})
export class HistoricPage {
  historique: AngularFireList<any>;
  historiquedirecteur: AngularFireList<any>;
  historiquetechnocomercial: AngularFireList<any>;
  public nm:number
  public totale:number
  histo: Observable<any[]>;
  histodirecteur:Observable<any[]>;
  private afAuth: AngularFireAuth
  historiquetechnoco:Observable<any[]>;
  constructor( private alertCtrl: AlertController,public navParams: NavParams,public navCtrl: NavController,
    public af: AngularFireDatabase) {
    this.historique =  af.list('/histo')
    this.historiquedirecteur =  af.list('/histodirecteur')
    this.nm=1 ;
      this.histo= this.historique.snapshotChanges();
 this.histodirecteur=this.historiquedirecteur.snapshotChanges();
 this.historiquetechnocomercial =  af.list('/historiquetechnoco')
 this.historiquetechnoco=this.historiquetechnocomercial.snapshotChanges();
    console.log(this.histo);

    



    }
 

question:number=0;


deleteEmployee(key: string)
{



   this.historique.remove(key) ;
  
  }
editPerson(key,login,password) {

    {
  console.log( key, login,  password)
   this.navCtrl.push(AddclientcommercialPage,{
   key : key, 
  login : login,
   password : password,
       });
      
      }

}  




addThemFunction (totale) {
     this.histo.subscribe(result => {
      console.log(result.length)   

  this.totale = this.nm+131+result.length  ;
  
});
   
   }
  
}


