
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, App } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { fireb } from '../../providers/firebase';


/**
 * Generated class for the MenuPage2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-page2',
  templateUrl: 'menu-page2.html',
})
export class MenuPage2Page {

  rootPage: any;
  pages1 = [];
  username = '';

  // Reference to the side menus root nav
  @ViewChild(Nav) nav: Nav;
  lang:any;
  constructor(public navCtrl: NavController, private authProvider: fireb, private appCtrl: App,
    public translate: TranslateService)
   {     this.lang = 'en';
   this.translate.setDefaultLang('en');
   this.translate.use('en');}

  

  ionViewWillEnter() {
    if (this.authProvider.isAdmin()) {
      this.pages1 = [
        { title: 'permission for  Technical Sales', page: 'TechnococommercialPage', icon: 'home' },
      { title: 'permission for director', page: 'DirecteurPage', icon: 'planet' },
      { title: 'permission for commercial ', page: 'PermissionPage', icon: 'planet' },

      ];

     
      this.openPage('PermissionPage');        }
     else {
      this.pages1 = [
        { title: 'User  Information ', page: 'UserPage', icon: 'home' },
        { title: 'technical sales ', page: 'LoginPage', icon: 'planet' },
        { title: 'director', page: 'LoginuserPage', icon: 'planet' },
        { title: 'commercial', page: 'LogincommerPage', icon: 'planet' }
      ];
      this.openPage('UserPage');
    }
    this.username = this.authProvider.currentUser.name;
  }
   
 
  logout() {
    this.authProvider.logout();
    this.appCtrl.getRootNav().setRoot('DebutloginPage');
  }

  openPage(page) {
    this.nav.setRoot(page);
  }

  ionViewCanEnter() {
    return this.authProvider.isLoggedIn();
  }

}