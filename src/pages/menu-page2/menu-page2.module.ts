import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuPage2Page } from './menu-page2';

@NgModule({
  declarations: [
    MenuPage2Page,
  ],
  imports: [
    IonicPageModule.forChild(MenuPage2Page),
  ],
})
export class MenuPage2PageModule {}
